<GameFile>
  <PropertyGroup Name="MenuScene" Type="Scene" ID="695d7f5c-7e8a-470a-8464-994bd8f51df6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="9" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="menu_1" ActionTag="-162714302" Tag="10" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" RightMargin="-640.0000" TopMargin="-139.4500" BottomMargin="-120.5500" ctype="SpriteObjectData">
            <Size X="1600.0000" Y="900.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="800.0000" Y="329.4500" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8333" Y="0.5148" />
            <PreSize X="1.6667" Y="1.4063" />
            <FileData Type="Normal" Path="shopDrash/menu.jpg" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_about" ActionTag="-1328845893" Alpha="0" Tag="11" IconVisible="False" LeftMargin="316.6458" RightMargin="597.3542" TopMargin="498.1027" BottomMargin="105.8974" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="339.6458" Y="123.8974" />
            <Scale ScaleX="10.4744" ScaleY="3.8890" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3538" Y="0.1936" />
            <PreSize X="0.0479" Y="0.0562" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_exit" ActionTag="-130310799" Alpha="0" Tag="12" IconVisible="False" LeftMargin="1252.6461" RightMargin="-338.6461" TopMargin="498.1036" BottomMargin="105.8964" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1275.6461" Y="123.8964" />
            <Scale ScaleX="10.2590" ScaleY="3.7514" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.3288" Y="0.1936" />
            <PreSize X="0.0479" Y="0.0562" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_play" ActionTag="802827548" Alpha="0" Tag="13" IconVisible="False" LeftMargin="789.5876" RightMargin="124.4124" TopMargin="470.8764" BottomMargin="133.1236" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="812.5876" Y="151.1236" />
            <Scale ScaleX="5.8448" ScaleY="7.3282" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8464" Y="0.2361" />
            <PreSize X="0.0479" Y="0.0562" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>