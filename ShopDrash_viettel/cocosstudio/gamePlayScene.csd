<GameFile>
  <PropertyGroup Name="gamePlayScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="main_1" ActionTag="-1379167424" Tag="6" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="-0.0320" RightMargin="-639.9680" TopMargin="-259.9840" BottomMargin="-0.0160" ctype="SpriteObjectData">
            <Size X="1600.0000" Y="900.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="799.9680" Y="449.9840" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8333" Y="0.7031" />
            <PreSize X="1.6667" Y="1.4063" />
            <FileData Type="Normal" Path="shopDrash/main.jpg" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_map" ActionTag="-1150861773" Tag="7" IconVisible="False" LeftMargin="27.1713" RightMargin="-167.1713" TopMargin="-216.1790" BottomMargin="86.1790" ctype="GameMapObjectData">
            <Size X="1100.0000" Y="770.0000" />
            <Children>
              <AbstractNodeData Name="bgChoice" ActionTag="780449570" Tag="13" IconVisible="False" LeftMargin="423.2403" RightMargin="536.7597" TopMargin="314.0999" BottomMargin="315.9001" ctype="SpriteObjectData">
                <Size X="140.0000" Y="140.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="493.2403" Y="385.9001" />
                <Scale ScaleX="0.8340" ScaleY="0.7983" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4484" Y="0.5012" />
                <PreSize X="0.1273" Y="0.1818" />
                <FileData Type="Normal" Path="shopDrash/choose.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="27.1713" Y="86.1790" />
            <Scale ScaleX="1.0448" ScaleY="1.0320" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0283" Y="0.1347" />
            <PreSize X="1.1458" Y="1.2031" />
            <FileData Type="Normal" Path="map_shop_drash.tmx" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="lb_time" ActionTag="327653415" Tag="8" IconVisible="False" LeftMargin="1343.5719" RightMargin="-570.5719" TopMargin="56.4791" BottomMargin="481.5209" IsCustomSize="True" FontSize="100" LabelText="0" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="187.0000" Y="102.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1437.0719" Y="532.5209" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.4969" Y="0.8321" />
            <PreSize X="0.1948" Y="0.1594" />
            <FontResource Type="Normal" Path="UVNBAISAU_N .TTF" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="lb_money" ActionTag="-1665015866" Tag="10" IconVisible="False" LeftMargin="1327.5720" RightMargin="-586.5720" TopMargin="188.3140" BottomMargin="349.6860" IsCustomSize="True" FontSize="100" LabelText="0" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="219.0000" Y="102.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1437.0720" Y="400.6860" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.4970" Y="0.6261" />
            <PreSize X="0.2281" Y="0.1594" />
            <FontResource Type="Normal" Path="UVNBAISAU_N .TTF" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_play_again" ActionTag="383853104" Alpha="0" Tag="11" IconVisible="False" LeftMargin="1373.5447" RightMargin="-459.5447" TopMargin="358.5499" BottomMargin="245.4501" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1396.5447" Y="263.4501" />
            <Scale ScaleX="8.1686" ScaleY="3.2922" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.4547" Y="0.4116" />
            <PreSize X="0.0479" Y="0.0562" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_back" ActionTag="1366320194" Alpha="0" Tag="12" IconVisible="False" LeftMargin="1376.5125" RightMargin="-462.5125" TopMargin="496.7307" BottomMargin="107.2693" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1399.5125" Y="125.2693" />
            <Scale ScaleX="8.1686" ScaleY="3.2922" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.4578" Y="0.1957" />
            <PreSize X="0.0479" Y="0.0562" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>