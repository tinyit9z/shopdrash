/**
 * Created by Huy on 3/27/16.
 */
var playGame = cc.LayerColor.extend({
    title_map: null,
    layer_map: null,
    arrAS: null,
    currPosXChoice: null,
    currPosYChoice: null,
    rmvArr: null,
    bgChoice: null,
    fChoice: null,
    sChoice: null,
    cUpdatePos: null,
    cChangePosFinish: null,
    isCheckUpdate: null,
    isChangePos:null,
    btn_play_again:null,
    btn_back:null,
    lb_time:null,
    lb_money:null,
    cTime:null,
    cMoney:null,
    self: this,
    ctor: function () {
        //////////////////////////////
        // 1. super init first
        this._super();
        this.setColor(cc.color(51, 51, 51));
        SharedData.gamePlayCurrentLayer = MAIN_LAYERS.LAYER_GAME_PLAY;
        this.arrAS = [];
        this.rmvArr = [];
        this.currPosXChoice=Math.floor(NUMBER_COLUNM/2);
        this.currPosYChoice=Math.floor(NUMBER_ROWS/2);
        this.cTime=0;
        this.cMoney=0;
        this.isChangePos=true;
        this.isCheckUpdate=false;
        this.sumHelp = 3;
        this.limitTime = 200;
        this.cScore = 0;
        this.loadSceneFromCocos();
        self = this;
        var listener1 = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                //Check the click area
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    //cc.log("sprite began... x = " + locationInNode.x + ", y = " + locationInNode.y);
                    target.opacity = 180;
                    var mapWidth = self.title_map.getContentSize().width;
                    var mapHeight = self.title_map.getContentSize().height+(self.title_map.getScaleY()-1)*self.title_map.getContentSize().height;
                    var tileWidth = self.title_map.getTileSize().width+(self.title_map.getScaleX()-1)*self.title_map.getTileSize().width;
                    var posMap = self.title_map.getPosition();
                    var tileHeight = self.title_map.getTileSize().width+(self.title_map.getScaleY()-1)*self.title_map.getTileSize().height;
                    var tit_Y = Math.floor((GC.h - locationInNode.y - (GC.h - posMap.y - mapHeight)) / tileHeight);
                    var tit_X = Math.floor((locationInNode.x - posMap.x) / tileWidth);
                    if ((tit_X >= 0 && tit_X < NUMBER_COLUNM) && (tit_Y >= 0 && tit_Y < NUMBER_ROWS)) {
                        //cc.log(self.getASAt(tit_X, tit_Y).type);
                    }
                    return true;
                }
                return false;
            },
            //Process the touch end event
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                //Check the click area
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    //cc.log("sprite began... x = " + locationInNode.x + ", y = " + locationInNode.y);
                    target.opacity = 180;
                    var mapWidth = self.title_map.getContentSize().width;
                    var mapHeight = self.title_map.getContentSize().height+(self.title_map.getScaleY()-1)*self.title_map.getContentSize().height;
                    var tileWidth = self.title_map.getTileSize().width+(self.title_map.getScaleX()-1)*self.title_map.getTileSize().width;
                    var posMap = self.title_map.getPosition();
                    var tileHeight = self.title_map.getTileSize().width+(self.title_map.getScaleY()-1)*self.title_map.getTileSize().height;
                    var tit_Y = Math.floor((GC.h - locationInNode.y - (GC.h - posMap.y - mapHeight)) / tileHeight);
                    var tit_X = Math.floor((locationInNode.x - posMap.x) / tileWidth);
                    if (tit_X >= 0 && tit_X < NUMBER_COLUNM && (tit_Y >= 0 && tit_Y < NUMBER_ROWS)) {
                        if (self.fChoice == null) {
                            self.fChoice = self.getASAt(tit_X, tit_Y);
                            self.bgChoice.setVisible(true);
                            self.bgChoice.setPosition(self.getPosAt(tit_X, tit_Y));
                            self.currPosXChoice=tit_X;
                            self.currPosYChoice=tit_Y;
                        }
                        else {
                            self.bgChoice.setVisible(false);
                            self.sChoice = self.getASAt(tit_X, tit_Y);
                            if (self.fChoice != self.sChoice)
                                self.checkResole(tit_X, tit_Y);
                            else {
                                self.fChoice = null;
                                self.sChoice = null;
                            }
                        }

                    }

                }
            }
        });
        cc.eventManager.addListener(listener1, self);
        this.bindEvent();
        return true;
    }
});
playGame.prototype.loadSceneFromCocos = function () {
    var mainscene = ccs.load(res.GamePlayScene);
    this.rootNode = mainscene.node;
    this.addChild(this.rootNode);
    this.title_map = this.rootNode.getChildByName("title_map");
    this.layer_map = this.title_map.getLayer("map_player");
    this.bgChoice = this.title_map.getChildByName("bgChoice");
    this.bgChoice.setVisible(false);
    this.btn_back=this.rootNode.getChildByName("btn_back");
    this.btn_back.addTouchEventListener(this.touchEvent,this);
    this.btn_play_again=this.rootNode.getChildByName("btn_play_again");
    this.btn_play_again.addTouchEventListener(this.touchEvent,this);
    this.lb_time=this.rootNode.getChildByName("lb_time");
    this.lb_money=this.rootNode.getChildByName("lb_money");
    for (var i = 0; i < NUMBER_COLUNM; i++) {
        for (var j = 0; j < NUMBER_ROWS; j++) {
            this.layer_map.getTileAt(i, j).setVisible(false);
        }
    }
    this.schedule(this.updateTime,1);
    this.loadMap();
}
playGame.prototype.updateTime=function(){
    this.cTime++;
    this.lb_time.setString(""+this.cTime);
}
playGame.prototype.loadMap = function () {
    this.arrAS = [];
    this.lb_money.setString(""+this.cMoney);
    this.lb_time.setString(""+this.cTime);
    for (var i = 0; i < NUMBER_COLUNM; i++) {
        var c=0;
        for (var j = 0; j < NUMBER_ROWS; j++) {
            c++;
            var radTypeAS = Math.floor(Math.random() * 5 + 1);
            var radIdAS = Math.floor(Math.random() * 5 + 1);
            var as = new myAS(radTypeAS, radIdAS);
            var tempAs=this.getPosAt(i, j);
            //as.setPosition(tempAs.x,this.layer_map.getContentSize().height+as.getContentSize().height*c);
            as.setPosition(tempAs);
            as._tempPos = tempAs;
            as._x = i;
            as._y = j;
            this.arrAS.push(as);
        }
    }
    this.mvSameTypeBeside(true);
    for (var i = 0; i < this.arrAS.length; i++) {
        this.title_map.addChild(this.arrAS[i]);
        //this.arrAS[i].setPosition(this.arrAS[i]._tempPos);
        //this.arrAS[i].runAction(cc.EaseBounceOut.create(cc.MoveTo.create(effectSpeed,this.arrAS[i]._tempPos)));
    }
}
playGame.prototype.checkResole = function () {
    cc.log("check");
    if(this.isChangePos)
    this.changePos();
}
playGame.prototype.updatePos = function () {
    for (var i = 0; i < NUMBER_COLUNM; i++) {//close up horizeltal
        var startX, startY;
        var startPos = null;
        for (var j = NUMBER_ROWS - 1; j >= 0; j--) {
            var mons = this.getASAt(i, j);
            if (mons == null && startPos == null) {
                startPos = this.getPosAt(i, j);
                startX = i;
                startY = j;
            }
            else if (mons != null && startPos != null) {
                mons._x = startX;
                mons._y = startY;
                //mons.setPosition(startPos);
                mons._tempPos = startPos;
                i--;
                break;
            }
        }
    }
    //fill as in map
    for(var i=0;i<NUMBER_COLUNM;i++){
        var c=0;
        for(var j=NUMBER_ROWS-1;j>=0;j--){
                if(this.getASAt(i,j)==null){
                    cc.log("crate");
                    c++;
                    var radTypeAS = Math.floor(Math.random() * 5 + 1);
                    var radIdAS = Math.floor(Math.random() * 5 + 1);
                    var as = new myAS(radTypeAS, radIdAS);
                    var spPos = this.getPosAt(i,j);
                    as.setPosition(spPos.x,this.title_map.getContentSize().height+as.getContentSize().height*c);
                    as._tempPos = spPos;
                    as._x = i;
                    as._y = j;
                    this.title_map.addChild(as);
                    this.arrAS.push(as);
                }
            }
        }
    this.cUpdatePos = this.arrAS.length;
    cc.log("Chieu dai mang:" + this.arrAS.length);
    for (var i = 0; i < this.arrAS.length; i++) {
        this.arrAS[i].runAction(cc.sequence(cc.EaseBounceOut.create(cc.MoveTo.create(effectSpeed, this.arrAS[i]._tempPos)), cc.callFunc(this.finishUpdatePos)));
    }
    cc.log("update pos");
}
playGame.prototype.finishUpdatePos = function () {
    cc.log("onto finish update Pos");
    self.cUpdatePos--;
    if (self.cUpdatePos == 0) {
        self.mvSameTypeBeside(false);
    }
}
playGame.prototype.changePos = function () {
    if (this.fChoice != null && this.sChoice != null) {
        var minusX = Math.abs(this.fChoice._x - this.sChoice._x);
        var minusY = Math.abs(this.fChoice._y - this.sChoice._y);
        if ((minusX == 1 && minusY == 0) || (minusX == 0 && minusY == 1)) {
            this.cChangePosFinish = 2;//count to certifi finish change position fchoice and schoice
           // this.isCheckUpdate = false;//update or none
            this.isChangePos=false;
            var tg = this.fChoice.getPosition();
            var tgX = this.fChoice._x;
            var tgY = this.fChoice._y;
            var tgtempPos = this.fChoice._tempPos;
            this.fChoice.runAction(cc.sequence(cc.EaseBackOut.create(cc.MoveTo.create(1, this.sChoice.getPosition())),cc.callFunc(this.finishChangePos)));
            //this.fChoice.setPosition(this.sChoice.getPosition());
            this.fChoice._x = this.sChoice._x;
            this.fChoice._y = this.sChoice._y;
            this.fChoice._tempPos = this.sChoice._tempPos;
            this.sChoice.runAction(cc.sequence(cc.EaseBackOut.create(cc.MoveTo.create(1, tg)),cc.callFunc(this.finishChangePos)));
            //this.sChoice.setPosition(tg);
            this.sChoice._x = tgX;
            this.sChoice._y = tgY;
            this.sChoice._tempPos = tgtempPos;

        }
    }
    this.resetChoice();
}
playGame.prototype.finishChangePos = function () {
    self.cChangePosFinish--;
    if (self.cChangePosFinish == 0) {
        cc.log("finish change pos")
        self.mvSameTypeBeside(false);
    }
}
playGame.prototype.mvSameTypeBeside = function (isCheck) {
    //check horizeltal
    this.rmvArr = [];
    var isMove = false;
    var c = 1;
    for (var i = 0; i < NUMBER_ROWS; i++) {
        for (var j = 0; j < NUMBER_COLUNM - 1; j++) {
            //cc.log("remove:"+j);
            var as1 = this.getASAt(j, i);
            if (as1 != null)
                for (var k = j + 1; k < NUMBER_COLUNM; k++) {
                    var as2 = this.getASAt(k, i);
                    if (as2 != null) {
                        if (as1.type == as2.type) {
                            c++;
                        }
                        else {
                            if (c >= 3) {
                                for (var f = j; f < k; f++) {
                                    var as = this.getASAt(f, i);
                                    if (!this.checkExist(as))
                                        this.rmvArr.push(as);
                                }
                                j = k - 1;
                                c = 1;
                                isMove = true;
                            }
                            else {
                                j = k - 1;
                                c = 1;
                            }
                            break;
                        }
                        if (k == NUMBER_COLUNM - 1) {
                            if (c >= 3) {
                                for (var f = j; f <= k; f++) {
                                    var as = this.getASAt(f, i);
                                    if (!this.checkExist(as))
                                        this.rmvArr.push(as);
                                }
                                c = 1;
                                isMove = true;
                            }
                            else {
                                c = 1;
                            }
                            break;
                        }
                    }
                    else {
                        if (c >= 3) {
                            for (var f = j; f <= k; f++) {
                                var as = this.getASAt(f, i);
                                if (!this.checkExist(as))
                                    this.rmvArr.push(as);
                            }
                            c = 1;
                            j = k;
                            isMove=true;
                        }
                        else {
                            c = 1;
                        }
                        break;
                    }

                }
        }
    }
    //check vertical
    c = 1;
    for (var i = 0; i < NUMBER_COLUNM; i++) {
        for (var j = 0; j < NUMBER_ROWS - 1; j++) {
            //cc.log("remove:"+j);
            var as1 = this.getASAt(i, j);
            if (as1 != null)
                for (var k = j + 1; k < NUMBER_ROWS; k++) {
                    var as2 = this.getASAt(i, k);
                    if (as2 != null) {
                        if (as1.type == as2.type) {
                            c++;
                        }
                        else {
                            if (c >= 3) {
                                for (var f = j; f < k; f++) {
                                    var as = this.getASAt(i, f);
                                    if (!this.checkExist(as))
                                        this.rmvArr.push(as);
                                }
                                j = k - 1;
                                c = 1;
                                isMove = true;
                            }
                            else {
                                j = k - 1;
                                c = 1;
                            }
                            break;
                        }
                        if (k == NUMBER_ROWS - 1) {//if k in terminal cell of row
                            if (c >= 3) {
                                for (var f = j; f <= k; f++) {
                                    var as = this.getASAt(i, f);
                                    if (!this.checkExist(as))
                                        this.rmvArr.push(this.getASAt(i, f));
                                }
                                c = 1;
                                isMove = true;
                            }
                            else {
                                c = 1;
                            }
                            break;
                        }
                    }
                    else {
                        if (c >= 3) {
                            for (var f = j; f <= k; f++) {
                                var as = this.getASAt(f, i);
                                if (!this.checkExist(as))
                                    this.rmvArr.push(as);
                            }
                            c = 1;
                            j = k;
                            isMove=true;
                        }
                        else {
                            c = 1;
                        }
                        break;
                    }
                }
        }
    }
    if (isCheck) {//check and replace cell may able remove
        if (this.rmvArr.length > 0) {
            for (var i = 0; i < this.rmvArr.length; i++) {
                var asmv = this.rmvArr[i];
                var asRep = new myAS(Math.floor(Math.random() * 5 + 1), Math.floor(Math.random() * 5 + 1));
                asRep.setPosition(asmv.getPosition());
                asRep._x = asmv._x;
                asRep._y = asmv._y;
                asRep._tempPos = asmv.getPosition();
                this.arrAS[this.arrAS.indexOf(asmv)] = asRep;
            }
            this.rmvArr = [];
            this.mvSameTypeBeside(true);
        }
    }
    if (isMove && !isCheck) {// if not check mode and able remove
        if(this.rmvArr.length>0) {
            this.countMoney(this.rmvArr.length);
            for (var i = 0; i < this.rmvArr.length; i++) {
                if (this.rmvArr[i] != null) {
                    this.rmvArr[i].activeRemove();
                    this.arrAS.splice(this.arrAS.indexOf(this.rmvArr[i]), 1);
                }
            }
        }//update pos again after remove as
        this.rmvArr = [];
        this.updatePos();
    }
    else if(!isMove&&!isCheck){
        this.isChangePos=true;// permiss change pos of gem when finish updata position
    }
    this.rmvArr = [];
    return isMove;
}
playGame.prototype.countMoney = function (value) {
    this.cMoney+=value;
    this.lb_money.setString(""+this.cMoney);
}
playGame.prototype.checkExist = function (obj) {
    for (var i = 0; i < this.rmvArr.length; i++) {
        if (this.rmvArr[i] == obj) {
            return true;
        }
    }
    return false;
}
playGame.prototype.resetChoice = function () {
    this.fChoice = null;
    this.sChoice = null;
}
playGame.prototype.getPosAt = function (x, y) {
    var pos = null;
    var sp = this.layer_map.getTileAt(x, y);
    if (sp != null) {
        pos = cc.p(sp.getPositionX() + sp.getContentSize().width / 2, sp.getPositionY() + sp.getContentSize().height / 2);
    }
    return pos;
}
playGame.prototype.getASAt = function (x, y) {
    for (var i = 0; i < this.arrAS.length; i++) {
        if (this.arrAS[i]._x == x && this.arrAS[i]._y == y) {
            return this.arrAS[i];
        }
    }
    return null;
}
playGame.prototype.touchEvent = function (sender, type) {
    var self = this;
    self.bgChoice.setVisible(false);
    switch (type) {
        case ccui.Widget.TOUCH_BEGAN:
            cc.log("Touch began");
            break;
        case ccui.Widget.TOUCH_ENDED:
            if (self.canClick == false) return;
            if (sender == self.btn_back) {
                self.backMenu();
            }
            if (sender == self.btn_play_again) {
                self.playAgain();
            }
            break;
    }
}
playGame.prototype.bindEvent = function () {
    var self = this;
    cc.eventManager.addListener({
        event: cc.EventListener.KEYBOARD,
        onKeyReleased: function (key, event) {

            if (SharedData.gamePlayCurrentLayer == MAIN_LAYERS.LAYER_GAME_PLAY) {
                self.bgChoice.setVisible(true);
                switch (key) {
                    //case GC.KEY_CODE.ENTER:self.goLevelScense();break;
                    case GC.KEY_CODE.NUM_1:
                        self.playAgain();
                        break;
                    case GC.KEY_CODE.NUM_2:
                        self.backMenu();
                        break;
                    case GC.KEY_CODE.UP:
                        self.moveUp();
                        break;
                    case GC.KEY_CODE.DOWN:
                        self.moveDown();
                        break;
                    case GC.KEY_CODE.LEFT:
                        self.moveleft();
                        break;
                    case GC.KEY_CODE.RIGHT:
                        self.moveRight();
                        break;
                    case GC.KEY_CODE.ENTER:
                        self.enter();
                        break;
                    default :
                        break;
                }
            }
        }

    }, this);
}
playGame.prototype.enter=function(){
    if(this.fChoice==null){
        this.fChoice=this.getASAt(this.currPosXChoice,this.currPosYChoice);
    }
    else{
        this.sChoice=this.getASAt(this.currPosXChoice,this.currPosYChoice);
        if(this.fChoice!=this.sChoice){
            this.checkResole();
        }
    }
}
playGame.prototype.moveUp=function(){
    if(this.currPosYChoice>0){
        this.currPosYChoice--;
        this.bgChoice.setPosition(this.getPosAt(this.currPosXChoice,this.currPosYChoice));

    }
}
playGame.prototype.moveDown=function(){
    if(this.currPosYChoice<NUMBER_ROWS-1){
        this.currPosYChoice++;
        this.bgChoice.setPosition(this.getPosAt(this.currPosXChoice,this.currPosYChoice));

    }
}
playGame.prototype.moveleft=function(){
    if(this.currPosXChoice>0){
        this.currPosXChoice--;
        this.bgChoice.setPosition(this.getPosAt(this.currPosXChoice,this.currPosYChoice));

    }
}
playGame.prototype.moveRight=function(){
    if(this.currPosXChoice<NUMBER_COLUNM-1){
        this.currPosXChoice++;
        this.bgChoice.setPosition(this.getPosAt(this.currPosXChoice,this.currPosYChoice));

    }
}
playGame.prototype.exitGame = function () {
    cc.log("Thoat game ");
    cc.director.end();
}
playGame.prototype.playAgain=function(){
    this.cTime=0;
    this.cMoney=0;
    for(var i=0;i<this.arrAS.length;i++){
        this.arrAS[i].removeFromParent();
    }
    this.loadMap();
}
playGame.prototype.backMenu=function(){

    cc.director.runScene(cc.TransitionFade.create(0.5,new HomeScense));
}
playGame.prototype.showGuild = function () {
    cc.log("Gioi thieu ve game");
}
var GamePlayScense = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new playGame();
        this.addChild(layer);
    }
});