/**
 * Created by Huy on 3/29/16.
 */
var homeLayer = cc.LayerColor.extend({
    self: this,
    btn_exit:null,
    btn_play:null,
    btn_about:null,
    ctor: function () {
        //////////////////////////////
        // 1. super init first
        this._super();
        this.setColor(cc.color(51, 51, 51));
        SharedData.homeCurrentLayer = HOME_LAYERS.LAYER_HOME;
        this.loadSceneFromCocos();
        self = this;
        this.bindEvent();
        return true;
    }
});
homeLayer.prototype.loadSceneFromCocos = function () {
    var mainscene = ccs.load(res.HomeScene_json);
    this.rootNode = mainscene.node;
    this.addChild(this.rootNode);
    this.btn_play=this.rootNode.getChildByName("btn_play");
    this.btn_play.addTouchEventListener(this.touchEvent,this);
    this.btn_exit=this.rootNode.getChildByName("btn_exit");
    this.btn_exit.addTouchEventListener(this.touchEvent,this);
    this.btn_about=this.rootNode.getChildByName("btn_about");
    this.btn_about.addTouchEventListener(this.touchEvent,this);
}
homeLayer.prototype.touchEvent = function (sender, type) {
    var self = this;
    switch (type) {
        case ccui.Widget.TOUCH_BEGAN:
            cc.log("Touch began");
            break;
        case ccui.Widget.TOUCH_ENDED:
            if (sender == self.btn_exit) {
                self.exitGame();
            }
            if (sender == self.btn_play) {
                self.playGame();
            }
            if (sender == self.btn_about) {
                self.showGuild();
            }
            break;
    }
}
homeLayer.prototype.bindEvent = function () {
    var self = this;
    cc.eventManager.addListener({
        event: cc.EventListener.KEYBOARD,
        onKeyReleased: function (key, event) {

            if (SharedData.homeCurrentLayer == HOME_LAYERS.LAYER_HOME)
                switch (key) {
                    //case GC.KEY_CODE.ENTER:self.goLevelScense();break;
                    case GC.KEY_CODE.NUM_1:
                        self.showGuild();
                        break;
                    case GC.KEY_CODE.NUM_2:
                        self.exitGame();
                        break;
                    case GC.KEY_CODE.ENTER:
                        self.playGame();
                        break;
                }
        }

    }, this);
}
homeLayer.prototype.exitGame = function () {
    cc.director.end();
}
homeLayer.prototype.playGame = function () {

    var fadeLayer = cc.TransitionFade.create(1,new GamePlayScense);
    cc.director.runScene(fadeLayer);
}
homeLayer.prototype.showGuild = function () {
}
var HomeScense = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new homeLayer();
        this.addChild(layer);
    }
});