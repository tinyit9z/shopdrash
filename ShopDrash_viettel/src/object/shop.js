/**
 * Created by Huy on 3/27/16.
 */
var accessory_type={
    "accessory":1,
    "bag":2,
    "hat":3,
    "lip":4,
    "shoe":5
}
var getAcessory=function(type,id){
    var accessory_name=null;
    switch (type){
        case accessory_type.accessory:
            accessory_name="accessory";
            break;
        case accessory_type.bag:
            accessory_name="bag";
            break;
        case accessory_type.shoe:
            accessory_name="shoe";
            break;
        case accessory_type.hat:
            accessory_name="hat";
            break;
        case accessory_type.lip:
            accessory_name="lip";
            break;
        default:
            accessory_name=accessory_type.hat;
            break;
    }
   return "res/shopDrash/Gem/"+accessory_name+id+"_normal.png";
}
var myAS=cc.Sprite.extend({
    type:null,
    _tempPos:null,
    id:null,
    _x:null,
    _y:null,
    self:null,
    isMoved:null,
    ctor:function(type,id){
        this._super(getAcessory(type,id));
        this.isMoved=false;
        this.setAnchorPoint(0.5,0.5);
        this.type=id;
        this.setScale(0.6);
    }
});
myAS.prototype.activeRemove=function(){
    var opaci = cc.fadeTo(effectSpeed,0);
    var scalDown = cc.scaleTo(0.3,0);
    this.self=this;
    this.runAction(cc.spawn(opaci,scalDown));
    this.scheduleOnce(this.remove,1);
}
myAS.prototype.remove=function(){
    this.removeFromParent();
}