/**
 * Created by nguyennhatanh on 10/17/15.
 */

//Home data
var SharedData = {};
SharedData.homeCurrentLayer = null;
var HOME_LAYERS = {
    LAYER_HOME : 0,
    LAYER_EXIT: 1,
    LAYER_ABOUT: 2,
}

SharedData.levelSelectCurrentLayer = null;
var LEVEL_SELECT_LAYERS = {
    LAYER_LEVEL_SELECT : 0,
}

SharedData.gamePlayCurrentLayer = null;
var MAIN_LAYERS = {
    LAYER_GAME_PLAY : 0,
    LAYER_WIN: 1,
    LAYER_LOSE: 2,
    LAYER_GUIDE:3,
}
SharedData.gameLevel = null;
var levelConfig = {
    crazyLevel:{
        sp_t1:20,
        sp_t2:20,
        sp_t3:20,
        sp_t4:20,
        sp_t5:16,
        rad:1000
    },
    hardLevel:{
        sp_t1:12,
        sp_t2:12,
        sp_t3:12,
        sp_t4:12,
        sp_t5:16,
        rad:500
    },
    normalLevel:{
        sp_t1:30,
        sp_t2:20,
        sp_t3:20,
        sp_t4:10,
        sp_t5:16,
        rad:200
    }

}
var NUMBER_COLUNM=10;
var NUMBER_ROWS=7;
var effectSpeed=2;